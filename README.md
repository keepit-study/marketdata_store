## Crypto Exchange MarketData Store With Python Async

CryptoCurrency Exchange Market Data Asynchronously Store with Python Asyncio & Aiohttp.

< Data Flow >

![System DataFlow](img/Dataflow.png)

### Preperations
- Redis
- MariaDB
- Haproxy
    : Loadblancer by each domains for Proxy servers, ssl pass-through on tcp mode.
- Proxy Servers by squid-proxy (At least more than 10 instances)
    : There is on the limitation of requests for each IP address.

### Proxy server Install & Setting at EC2 Ubuntu 16 instance
- edit `master ip` to your own machine at `squid_setup.sh`
- copy `squid_setup.sh` on Ubuntu 16.04 instance
- run `bash squid_setup.sh`
- Automatically installed squid server then reboot

### Sample Running Result

Bitfinex 204 Pairs Ungrouped/Grouped Orderbook data in 25 sec.<br> (Total requests 408 time via 100 proxy servers)<br>
Now It 254 Pairs on live at Bifinex exchange.<br>
![Sample Running](img/vgbf_ob_insert_runnig.gif)
<p>First Running was over 1 min due to making table on DB.

- Todo Features
    - [Redis DB](https://redis.io/) for getting Websocket data and to reshape Recent Orderbooks, Recent Tickers.
    - To be Cluster, Multi-node for DB, Requseters on docker swarm.