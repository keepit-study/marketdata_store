from datetime import datetime

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session

from sqlalchemy.dialects.mysql import DOUBLE, DATETIME
from sqlalchemy import Column, Integer, DateTime

from local_setting import CRYPTO_DB

engine = create_engine(CRYPTO_DB, convert_unicode=True, pool_recycle=600)

Base = declarative_base(bind=engine)
DBSession = scoped_session(sessionmaker(autocommit=False,
                                        autoflush=False,
                                        bind=engine))
session = DBSession()


class Tick_Base(Base):
    """ 
    Exchange Orderbook data Format
    """
    __abstract__ = True
    __mapper_args__= {'always_refresh': True}
    
    _id = Column(Integer, primary_key=True, autoincrement=True)
    close_price = Column(DOUBLE, nullable=False)
    trade_volume = Column(DOUBLE, nullable=False)
    ticker_time = Column(DATETIME(fsp=6), nullable=True)
    update_time = Column(DATETIME(fsp=6), nullable=True)
    
    def __init__(self):
        self.close_price = 0
        self.trade_volume = 0
        self.ticker_time = 0
        self.update_time = datetime.utcnow()


def init(obj):
    obj.metadata.create_all(engine)

    
def db_insert(obj):
    try:
        session.add(obj)
        session.commit()
    except Exception as e:
        session.rollback()
        raise
    finally:
        session.close()

        
def db_query(sql):
    conn = engine.connect()
    trans = conn.begin()
    try:
        result = conn.execute(sql)
    except Exception as e:
        trans.rollback()
        result = None
    finally:
        trans.close()

    return result

