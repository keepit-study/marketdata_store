import json
from datetime import datetime

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session

from sqlalchemy.dialects.mysql import DOUBLE, DATETIME
from sqlalchemy import Column, Integer, Text


from local_setting import CRYPTO_DB

engine = create_engine(CRYPTO_DB, convert_unicode=True, pool_recycle=600)

Base = declarative_base(bind=engine)
DBSession = scoped_session(sessionmaker(autocommit=False,
                                        autoflush=False,
                                        bind=engine))
session = DBSession()


class TradeHistory_Base(Base):
    """ 
    Exchange Orderbook data Format
    """
    __abstract__ = True
    __mapper_args__= {'always_refresh': True}
    
    _id = Column(Integer, primary_key=True)
    trade_time = Column(DATETIME(fsp=6), nullable=True)
    price = Column(DOUBLE, nullable=False)
    quantity = Column(DOUBLE, nullable=False)
    amount = Column(DOUBLE, nullable=False)
    update_time = Column(DATETIME(fsp=6), nullable=True)
    
    def __init__(self):
        self.trade_time = datetime.utcnow()
        self.price = 0
        self.quantity = 0
        self.amount = self.price + self.quantity # statement none cal on initialize
        self.update_time = datetime.utcnow()
        
    def __str__(self):
        return self._to_json()

    def _to_json(self):
        return json.dumps({
            'id': self._id,
            'trade_time': self.trade_time.strftime('%Y-%m-%d %H:%M:%S'),
            'price' : self.price,
            'quantity' : self.quantity,
            'amount' : self.amount,
            'update_time': self.update_time.strftime('%Y-%m-%d %H:%M:%S'),
        })
    

def init(obj):
    obj.metadata.create_all(engine)

    
def db_insert(obj):
    try:
        session.add(obj)
        session.commit()
    except Exception as e:
        session.rollback()
        raise
    finally:
        session.close()
        
        
def db_insert_bulk(objs: list):
    try:
        session.bulk_save_objects(objs)
        session.commit()
    except Exception as e:
        session.rollback()
        raise
    finally:
        session.close()
        
        
def db_query(sql):
    conn = engine.connect()
    trans = conn.begin()
    try:
        result = conn.execute(sql)
    except Exception as e:
        trans.rollback()
        result = None
    finally:
        trans.close()

    return result

