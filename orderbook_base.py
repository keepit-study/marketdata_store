import json
from datetime import datetime

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session

from sqlalchemy.dialects.mysql import DATETIME
from sqlalchemy import Column, Text, Integer

from local_setting import CRYPTO_DB

# CRYPTO_DB is MariaDB setting string
# Example > 
# local__setting.py
# CRYPTO_DB = "mysql+pymysql://[id]:[password]@1localhost:3306/crypto_finance"

engine = create_engine(CRYPTO_DB, convert_unicode=True, pool_recycle=600)

Base = declarative_base(bind=engine)
DBSession = scoped_session(sessionmaker(autocommit=False,
                                        autoflush=False,
                                        bind=engine))
session = DBSession()


class Orderbook_Base(Base):
    """ 
    Exchange Orderbook data Format
    """
    __abstract__ = True
    __mapper_args__= {'always_refresh': True}
    __table_args__ = {'mysql_engine': 'InnoDB',
                      'mysql_row_format': 'COMPRESSED',
                      'mysql_key_block_size': '8', 
                      'extend_existing': True}
    
    _id = Column(Integer, primary_key=True, autoincrement=True)
    timestamp = Column(DATETIME(fsp=6), nullable=True)
    asks_price = Column(Text, nullable=False)
    asks_volume = Column(Text, nullable=False)
    asks_amount = Column(Text, nullable=False)
    asks_amount_sum = Column(Text, nullable=False)
    asks_volume_sum = Column(Text, nullable=False)
    bids_price = Column(Text, nullable=False)
    bids_volume = Column(Text, nullable=False)
    bids_amount = Column(Text, nullable=False)
    bids_amount_sum = Column(Text, nullable=False)
    bids_volume_sum = Column(Text, nullable=False)
    update_time = Column(DATETIME(fsp=6), nullable=True)
    
    def __init__(self):
        self.timestamp = datetime.utcnow()
        self.asks_price = 'asks_price'
        self.asks_volume = 'asks_volume'
        self.asks_amount = 'asks_amount'
        self.asks_amount_sum = 'asks_amount_sum'
        self.asks_volume_sum = 'asks_volume_sum'
        self.bids_price = 'bids_price'
        self.bids_volume = 'bids_volume'
        self.bids_amount = 'bids_amount'
        self.bids_amount_sum = 'bids_amount_sum'
        self.bids_volume_sum = 'bids_volume_sum'
        self.update_time = datetime.utcnow()
        
    def __str__(self):
        return self._to_json()

    def _to_json(self):
        return json.dumps({
            'id': self._id,
            'timestamp': self.timestamp.strftime('%Y-%m-%d %H:%M:%S.%f'),
            'asks_price': self.asks_price,
            'asks_volume': self.asks_volume,
            'asks_amount': self.asks_amount,
            'asks_amount_sum': self.asks_amount_sum,
            'asks_volume_sum': self.asks_volume_sum,
            'bids_price': self.bids_price,
            'bids_volume': self.bids_volume,
            'bids_amount': self.bids_amount,
            'bids_amount_sum': self.bids_amount_sum,
            'bids_volume_sum': self.bids_volume_sum,
            'update_time': self.update_time.strftime('%Y-%m-%d %H:%M:%S.%f'),
        })
    

def init(obj):
    obj.metadata.create_all(engine)


def db_insert(obj):
    try:
        session.add(obj)
        session.commit()
    except Exception as e:
        session.rollback()
        raise
    finally:
        session.close()


def db_query(sql):
    conn = engine.connect()
    trans = conn.begin()
    try:
        result = conn.execute(sql)
    except Exception as e:
        trans.rollback()
        result = None
    finally:
        trans.close()

    return result

