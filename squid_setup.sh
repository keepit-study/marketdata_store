sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get install squid -y
sudo mv /etc/squid/squid.conf /etc/squid/squid.conf.bkp
echo "acl master src [your host ip] ## Must Edit
acl all src 0.0.0.0/0.0.0.0
http_access allow master
http_access deny all

visible_hostname $(hostname)

acl SSL_ports port 443
acl Safe_ports port 80          # http
acl Safe_ports port 21          # ftp
acl Safe_ports port 443         # https
acl Safe_ports port 70          # gopher
acl Safe_ports port 210         # wais
acl Safe_ports port 1025-65535  # unregistered ports
acl Safe_ports port 280         # http-mgmt
acl Safe_ports port 488         # gss-http
acl Safe_ports port 591         # filemaker
acl Safe_ports port 777         # multiling http
acl CONNECT method CONNECT

http_access deny !Safe_ports
http_access deny CONNECT !SSL_ports

http_access allow localhost manager
http_access deny manager

http_access allow localhost
http_access deny all

http_port 3128 ## Can adjust your port It's Default Port

no_cache deny all ## Does not necessary to store cache data

coredump_dir /var/spool/squid

refresh_pattern .               0       20%     4320
forwarded_for delete" > /etc/squid/squid.conf
sudo service squid restart
sudo reboot
