import os
import sys
import ujson
import pickle
import random
import logging
import asyncio
import aiohttp
from pathlib import Path
from datetime import datetime, timedelta

sys.path.append("../../")

import redis
from sqlalchemy.orm import sessionmaker, scoped_session
from tradehistory_base import engine, TradeHistory_Base, Column, Text

from local_setting import haproxy, AWS_PROXIES

"""
# This Version are requesting via HAproxy 
Requirements

- MariaDB installed
- HAproxy for loadbalancing and As many as Squid proxies
- Redis server installed #TODO
    : for update trading pair checking
"""


def insert_many(table_name, trades_data, dt):
    log = logging.getLogger("insert_many")
    dbsession = scoped_session(sessionmaker(autocommit=False,
                                            autoflush=False,
                                            bind=engine))
    session = dbsession()

    data_count = len(trades_data)

    try:
        session.bulk_save_objects(trades_data)
        session.commit()
        log.info("{} >> {}, {} rows Sent db sucess".format(dt, table_name, data_count))
    except Exception as e:
        session.rollback()
        log.error("{} >> {}, {} rows insert error".format(dt, table_name, data_count))
        log.error(e)
        raise
    finally:
        session.close()


async def data_process(pair, trade_data, dt):
    global _path
    log = logging.getLogger("data_process")
    """
    <trade_data sample>
    {'amount': '16.0649968',
     'exchange': 'bitfinex',
     'price': '6570.1',
     'tid': 266181431,
     'timestamp': 1530976643,
     'type': 'sell'}
    """
    # Table_Name format example : VGBF_TK_BTC_USD
    # V1에서는 Pair의 String이 6글자를 넘지 않는다.
    # Table 에서 v1 구분을 지어준다.
    table_name = TABLE_NAME.format("_".join([pair[:3].upper(), pair[-3:].upper()]))

    class Tradehistory(TradeHistory_Base):
        __tablename__ = table_name

        exchange = Column(Text)
        trade_type = Column(Text, nullable=False)

    try:
        with open(str(Path.joinpath(_path, "temp_data", "{}.pkl".format(table_name))), "rb") as last_file:
            tid_data = pickle.load(last_file)
            last_tid = max(tid_data)
            log.info("found file load last_tid : {}".format(last_tid))
    except Exception as exp:
        log.error(exp)
        last_tid = 0



    updated_data = [r for r in trade_data if r['tid'] > last_tid]
    log.info("newly found data : {}".format(len(updated_data)))

    if len(updated_data) == 0:
        # Theres no updated trade data close process as a return.
        return

    query_data = Tradehistory()

    # Check table then create if not exist.
    if table_name not in query_data.metadata.bind.table_names():
        query_data.metadata.create_all(engine)

    # Data List stacking on bulk_data
    bulk_data = []

    for _row in updated_data:
        query_data = Tradehistory()
        query_data._id = _row['tid']
        query_data.price = _row['price']
        query_data.amount = _row['amount']
        query_data.trade_time = datetime.fromtimestamp(_row['timestamp']) - timedelta(hours=9)
        query_data.exchange = _row['exchange']
        query_data.trade_type = _row['type']

        bulk_data.append(query_data)

    # save marker data as a sample
    with open(Path.joinpath(_path, "temp_data", "{}.pkl".format(table_name)), "wb") as new_file:
        pickle.dump([d['tid'] for d in trade_data], new_file)

    # await insert_many(table_name, bulk_data, dt)
    await loop.run_in_executor(None, insert_many, table_name, bulk_data, dt)


async def request_consume(main, dlq, session, results):
    log = logging.getLogger("request_consume")
    # TODO if re-try counter more than 2 time, switch port backup HAproxy
    while True:
        # wait for an item from the producer
        (pair, _try_count) = await main.get()

        if _try_count < 2:
            prox = haproxy['http'].format(11112)
        else:
            # TODO upper follow description, hardcoded proxy ips select until slave HAproxy
            prox = random.choice(AWS_PROXIES)['https']

        try:
            async with session.get(TRADEHISORY_URL.format(pair),
                                   params={"limit_trades": 999},
                                   proxy=prox) as resp:
                log.info("consuming request {}".format(resp.url))
                # async `json` method의 경우 content_type을 check해주는데, 여기서 에러가 날 수 있다.
                # Bitfinex 응답에는 Content_type을 application/json 이 포함되어 있어 aiohttp에서 에러나지 않음.
                data = resp.json(encoding='utf-8')
                _data = await data
                dt = datetime.utcnow()
                processed_data = await data_process(pair, _data, dt)

                # for raw data redis saving?
                results.append({"Raw_Data_{}".format(pair): _data,
                                "Processed_Data_{}".format(pair): processed_data})

                main.task_done()

        except Exception as exp:
            """
            retry count를 배치해서 3번이상 실패하면 dlq queue 라인에 있는 것을 종결처리해버린다.
            """
            log.error(exp)
            _try_count += 1

            if _try_count < 3:
                log.info("Queue Error at {}, re-try count {}".format(pair.upper(), _try_count))

                await dlq.put((pair, _try_count))
                await asyncio.sleep(3)
                main.task_done()
            else:
                log.error("It more than 3 times retry, Done it")
                dlq.task_done()


async def request_produce(queue, pair_list):
    log = logging.getLogger("request_produce")

    for pair in pair_list:
        log.info('producing {} request'.format(pair.upper()))
        # put the item in the queue
        await queue.put((pair, 0))  # 0 is retry counter


async def request_order(session, pair_list):
    log = logging.getLogger("request_order")
    main_queue = asyncio.Queue()
    dlq_queue = asyncio.Queue()
    results = []

    await request_produce(queue=main_queue, pair_list=pair_list)

    # len(aws_proxies) 의 이유는 이것이 semaphore와 같은 역할을 한다.
    # requests 날리는 타이밍이 달라진다.
    consumers = [asyncio.ensure_future(request_consume(main=main_queue,
                                                       dlq=dlq_queue,
                                                       session=session,
                                                       results=results)) for _ in range(int(len(AWS_PROXIES) / 1))]

    dlq_consumers = [asyncio.ensure_future(request_consume(main=dlq_queue,
                                                           dlq=dlq_queue,
                                                           session=session,
                                                           results=results)) for _ in range(int(len(AWS_PROXIES) / 2))]

    log.info("tesks all completed")
    await main_queue.join()
    await dlq_queue.join()
    log.info("all queue joined")
    for _future in consumers + dlq_consumers:
        _future.cancel()


async def run(pair_list):
    log = logging.getLogger("Run")
    try:
        async with aiohttp.ClientSession(headers={'User-Agent': "Python/3.6"}) as session:
            await request_order(session, pair_list)
    except Exception as exp:
        log.error(exp)

################################
# Main section
################################
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(name)18s : %(message)s', stream=sys.stderr)
logging.basicConfig(level=logging.ERROR, format='%(asctime)s %(name)18s : %(message)s', stream=sys.stderr)

if __name__ == '__main__':
    start_time = datetime.utcnow()
    # 크롤링 파일이 많아져 파일이름으로 Tablename 을 세팅하는것이 안전하다.
    (_, _file) = os.path.split(__file__)
    _path = Path.cwd()
    filename = [seg.upper() for seg in _file.split('_')]

    TRADEHISORY_URL = "https://api.bitfinex.com/v1/trades/{}"
    TABLE_NAME = "_".join(filename[:2]).upper() + "_v1_{}"  # syncing file name as table.

    rootlog = logging.getLogger()

    try: # get pair_set from local redis server
        r = redis.StrictRedis(host="localhost", port=6379, charset="utf-8", decode_responses=True, db=0)

        PAIR_LIST = r.smembers("vgbf_v1_pair:using_set")
        rootlog.info("all pairs count : {}".format(len(PAIR_LIST)))
    except Exception as e:
        rootlog.warning("redis db has problem", e)
        with open(Path.joinpath(_path, "pair_list.json"), "r", encoding='utf8') as pair_file:
            PAIR_LIST = ujson.loads(pair_file.read())

        rootlog.error("all pairs count : {} from 'pair_list.json'".format(len(PAIR_LIST)))

    rootlog.info("all pairs count : {}".format(len(PAIR_LIST)))
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(PAIR_LIST))

    # 후에 Redis서버에서 거래량을 비례하여 우선처리 순서를 정하면 좋을것 같다.

    end_time = datetime.utcnow()
    rootlog.info("total running time : {} ".format(str(end_time - start_time)))

