import sys
import time
import ujson
import redis
import logging
import requests

from datetime import datetime
sys.path.append("../../")

from local_setting import haproxy

"""
data process

1. request data
2. save raw data to 'vgbf_v1_pair:raw'
3. time tagging and update 'vgbf_v1_pair:lastupdate'
    3.1 check new from 'vgbf_v1_pair:using_set'
4. if new pair, then save 'vgbf_v1_pair:initupdate'
# TODO
5. recongnize expires, 
"""
time.sleep(30)  # to avoid workload on every o'minutes
log = logging.getLogger()

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s : %(message)s',
    stream=sys.stderr
)
# 1. requests data form sources
try:
    r = redis.StrictRedis(host="localhost", port=6379, charset="utf-8", decode_responses=True, db=0)
    using_pair_set = r.smembers("vgbf_v1_pair:using_set")
    log.info("found pairs from local redis : {}".format(len(using_pair_set)))
except Exception as e:
    log.debug(e)
    raise ConnectionError

try:
    resp = requests.get("https://api.bitfinex.com/v1/symbols_details",
                        proxies = {"http": haproxy['https'].format(11112)})
    if resp.status_code != 200:
        resp.raise_for_status()
    else:
        dt = datetime.now()
        data = ujson.loads(resp.text)
        log.info("found pairs from API end-point: {}".format(len(data)))
except Exception as e:
    log.debug(e)
    raise ConnectionError

# 2. save raw data 
raw_dict = {d['pair']:ujson.dumps(d) for d in data}
r.lpush("vgbf_v1_pair:raw_resp", resp.text)
r.hmset("vgbf_v1_pair:raw_dict", raw_dict)

# 3. time tagged data update
# 4. found new pair, then save 
time_tagged = {d['pair']:dt for d in data}
r.hmset("vgbf_v1_pair:lastupdate", time_tagged)
log.info("saved 'raw', 'dict', 'time_tagged' data")

updated_pairs = [d['pair'] for d in data if d['pair'] not in using_pair_set]

log.info("found new {} pair".format(len(updated_pairs)))
if updated_pairs:
    for pair in updated_pairs:
        r.sadd("vgbf_v1_pair:using_set", pair)
        r.hmset("vgbf_v1_pair:initupdate", {pair:dt})
        
        
# 5. found expiring pairs then remove using_set