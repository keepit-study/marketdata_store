import os
import sys
import ujson
import random
import asyncio
import aiohttp
import logging
import multiprocessing
from datetime import datetime, timedelta
from pathlib import Path

sys.path.append("../../")

import redis
from sqlalchemy.dialects.mysql import BOOLEAN
from sqlalchemy.orm import sessionmaker, scoped_session
from orderbook_base import Orderbook_Base, engine, Column
import numpy as np

from local_setting import haproxy, AWS_PROXIES

"""
# This Version are requesting via HAproxy 
Requirements

- MariaDB installed
- HAproxy for loadbalancing and As many as Squid proxies
- Redis server installed
    : for update trading pair checking
"""
(_path, _file) = os.path.split(__file__)
filename = [seg.upper() for seg in _file.split('_')]

TABLE_NAME = "_".join(filename[:2]).upper() + "_v1_{}"  # syncing file name as table.
ORDERBOOK_URL = "https://api.bitfinex.com/v1/book/{}"

GROUPED_MSG = {0: "Ungrouped", 1: "Grouped"}

# helper function
def array2str(array):
    reshape_array = np.array2string(array, separator=',',
                                    max_line_width=99999999,
                                    formatter={'float': lambda x: "%.12f" % x})
    return reshape_array


def insert_data(query_data):
    log = logging.getLogger("insert_data")
    DBSession = scoped_session(sessionmaker(autocommit=False,
                                            autoflush=False,
                                            bind=engine))
    session = DBSession()

    dt = datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f >> ")

    try:
        session.add(query_data)
        session.commit()
        log.info("{} {} data Sent db sucess".format(dt, query_data.__tablename__))
    except Exception as e:
        session.rollback()
        log.error("{} {} data insert error".format(dt, query_data.__tablename__))
        raise
    finally:
        session.close()


async def data_process(pair, data, orderbook_time, grouped):
    log = logging.getLogger("data_process")
    loop = asyncio.get_event_loop()
    try:
        ask_price = np.array([r['price'] for r in data['asks']], dtype='float')
        ask_volume = np.array([r['amount'] for r in data['asks']], dtype='float')
        ask_amount = ask_price * ask_volume

        bid_price = np.array([r['price'] for r in data['bids']], dtype='float')
        bid_volume = np.array([r['amount'] for r in data['bids']], dtype='float')
        bid_amount = bid_price * bid_volume

        reshape_data = {'ask_price': array2str(ask_price), 'ask_volume': array2str(ask_volume),
                        'ask_amount': array2str(ask_amount), 'bid_price': array2str(bid_price),
                        'bid_volume': array2str(bid_volume), 'bid_amount': array2str(bid_amount)}

        # Table_Name format example : VGBF_OB_BTC_USD
        # V1에서는 Pair의 String이 6글자를 넘지 않는다.
        Table_Name = TABLE_NAME.format("_".join([pair[:3].upper(), pair[-3:].upper()]))

        # Overide Grouped Ungoruped Column
        class Orderbook(Orderbook_Base):
            __tablename__ = Table_Name

            grouped = Column(BOOLEAN, nullable=False)

        query_data = Orderbook()

        # Check table then create if not exist.
        if Table_Name not in query_data.metadata.bind.table_names():
            query_data.metadata.create_all(engine)

        query_data.timestamp = orderbook_time
        query_data.asks_price = reshape_data['ask_price']
        query_data.asks_volume = reshape_data['ask_volume']
        query_data.asks_volume_sum = array2str(ask_volume.cumsum())
        query_data.asks_amount = reshape_data['ask_amount']
        query_data.asks_amount_sum = array2str(ask_amount.cumsum())

        query_data.bids_price = reshape_data['bid_price']
        query_data.bids_volume = reshape_data['bid_volume']
        query_data.bids_volume_sum = array2str(bid_volume.cumsum())
        query_data.bids_amount = reshape_data['bid_amount']
        query_data.bids_amount_sum = array2str(bid_amount.cumsum())

        query_data.grouped = grouped

        # insert_data is not a coroutine function
        await loop.run_in_executor(None, insert_data, query_data)
    except Exception as e:
        log.error(e)
        raise

    return reshape_data


async def request_consume(main, dlq, session, results):
    log = logging.getLogger("request_consume")

    while True:
        # wait for an item from the producer
        (pair, grouped, _try_count) = await main.get()


        if _try_count < 2:
            prox = haproxy['http'].format(11112)
        else:
            prox = random.choice(AWS_PROXIES)['https']

        try:
            # v1 API의 경우 ask, bid 모두 5000개 까지만 반환된다.
            # Group Parameter Detailed
            # If 1, orders are grouped by price in the orderbook. 
            # If 0, orders are not grouped and sorted individually
            async with session.get(ORDERBOOK_URL.format(pair),
                                   params={"group": grouped, "limit_bids": 5000, "limit_asks": 5000},
                                   proxy=prox) as resp:
                #print("consuming request {} {}".format(grouped_msg[grouped], resp.url))
                log.info("cunsuming request {} : {}".format(GROUPED_MSG[grouped], resp.url))
                # async `json` method의 경우 content_type을 check해주는데, 여기서 에러가 날 수 있다.
                # Bitfinex 응답에는 Content_type을 application/json 이 포함되어 있어 aiohttp에서 에러나지 않음.
                data = resp.json(encoding='utf-8')
                _data = await data
                # Timestamp organize
                try:
                    _dt = float(_data['asks'][0]['timestamp'])
                except:
                    _dt = float(_data['bids'][0]['timestamp'])
                finally:
                    dt = datetime.fromtimestamp(_dt) - timedelta(hours=9)

                #processed_data = await data_process(pair, _data, dt, grouped)
                await data_process(pair, _data, dt, grouped)
                # for raw data redis saving?
                """
                results.append({"Raw_Data_{}".format(pair): _data,
                                "Processed_Data_{}".format(pair): processed_data})
                """
                main.task_done()

        except Exception as e:
            """
            retry count를 배치해서 3번이상 실패하면 dlq queue 라인에 있는 것을 종결처리해버린다.
            """
            log.error(e)
            _try_count += 1

            if _try_count < 3:
                log.info("Queue Error at {} {}, re-try count {}".format(GROUPED_MSG[grouped],
                                                                        pair.upper(),
                                                                        _try_count))

                await dlq.put((pair, grouped, _try_count))
                await asyncio.sleep(3)
                main.task_done()
            else:
                log.error("It more than 3 times retry, Done it")
                # main.task_done()
                dlq.task_done()


async def request_produce(queue, pair_list, grouped):
    log = logging.getLogger("request_produce")

    for pair in pair_list:
        log.info('producing {} request'.format(pair.upper()))
        # put the item in the queue

        await queue.put((pair, grouped, 0))


async def request_order(session, pair_list, grouped):
    log = logging.getLogger("request_order")
    main_queue = asyncio.Queue()
    dlq_queue = asyncio.Queue()
    results = []

    await request_produce(queue=main_queue, pair_list=pair_list, grouped=grouped)

    # len(aws_proxies) 의 이유는 이것이 semaphore와 같은 역할을 한다.
    # requests 날리는 타이밍이 달라진다.
    consumers = [asyncio.ensure_future(request_consume(main=main_queue,
                                                       dlq=dlq_queue,
                                                       session=session,
                                                       results=results)) for _ in range(int(len(AWS_PROXIES) / 1))]

    dlq_consumers = [asyncio.ensure_future(request_consume(main=dlq_queue,
                                                           dlq=dlq_queue,
                                                           session=session,
                                                           results=results)) for _ in range(int(len(AWS_PROXIES) / 2))]

    log.info("tesks all completed")
    await main_queue.join()
    await dlq_queue.join()
    log.info("all queue joined")
    for _future in consumers + dlq_consumers:
        _future.cancel()


async def run(pair_list, grouped):
    log = logging.getLogger("Run")
    log.info("start running on {}".format(GROUPED_MSG[grouped]))
    # Headers not necessary
    async with aiohttp.ClientSession(headers={'User-Agent': "Python/3.6"}) as session:
        await request_order(session, pair_list, grouped)


def bootstrap(pair_list, grouped):
    log = logging.getLogger("Bootstrap run")
    start_time = datetime.utcnow()
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    log.info(GROUPED_MSG[grouped])
    loop.run_until_complete(run(pair_list, grouped))
    end_time = datetime.utcnow()
    log.info("end task, {} during {}".format(GROUPED_MSG[grouped], str(end_time - start_time)))


################################
# Main section
################################
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(name)18s : %(message)s', stream=sys.stderr)
logging.basicConfig(level=logging.ERROR, format='%(asctime)s %(name)18s : %(message)s', stream=sys.stderr)
# grouped arguments must be 1 (True) or 0 (False)
if __name__ == '__main__':
    # 크롤링 파일이 많아져 파일이름으로Tablename을 세팅하는것이 안전하다.
    rootlog = logging.getLogger()

    try: # get pair_set from local redis server
        r = redis.StrictRedis(host="localhost", port=6379, charset="utf-8", decode_responses=True, db=0)

        PAIR_LIST = r.smembers("vgbf_v1_pair:using_set")
        rootlog.info("all pairs count : {}".format(len(PAIR_LIST)))
    except Exception as e:
        rootlog.warning("redis db has problem", e)
        with open(Path.joinpath(_path, "pair_list.json"), "r", encoding='utf8') as pair_file:
            PAIR_LIST = ujson.loads(pair_file.read())[:10]

        rootlog.error("all pairs count : {} from 'pair_list.json'".format(len(PAIR_LIST)))

    multiprocessing.freeze_support()
    for group in [0, 1]:
        run_process = multiprocessing.Process(target=bootstrap, args=(PAIR_LIST, group,))
        run_process.start()

# 후에 Redis서버에서 거래량을 비례하여 우선처리 순서를 정하면 좋을것 같다.
