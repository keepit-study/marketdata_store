import os
import sys
import ujson
import random
import logging
import asyncio
import aiohttp
from datetime import datetime, timedelta
from pathlib import Path

sys.path.append("../../")

import redis
from sqlalchemy.orm import sessionmaker, scoped_session
from tick_base import Tick_Base, engine, Column, DOUBLE

from local_setting import haproxy, AWS_PROXIES

"""
# This Version are requesting via HAproxy 
Requirements

- MariaDB installed
- HAproxy for loadbalancing and As many as Squid proxies
- Redis server installed #TODO
    : for update trading pair checking
"""

def insert_data(query_data):
    log = logging.getLogger("insert_data")
    DBSession = scoped_session(sessionmaker(autocommit=False,
                                            autoflush=False,
                                            bind=engine))
    session = DBSession()

    dt = datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f >> ")

    try:
        session.add(query_data)
        session.commit()
        log.info("{} {} data Sent db sucess".format(dt, query_data.__tablename__))
    except Exception as e:
        session.rollback()
        log.error(e)
        log.error("{} {} data insert error".format(dt, query_data.__tablename__))
        raise
    finally:
        session.close()


async def data_process(pair, tick_data):
    log = logging.getLogger("data_process")
    try:
        # Table_Name format example : VGBF_TK_BTC_USD
        # V1에서는 Pair의 String이 6글자를 넘지 않는다.
        table_name = TABLE_NAME.format("_".join([pair[:3].upper(), pair[-3:].upper()]))

        class Tick(Tick_Base):
            __tablename__ = table_name

            mid_price = Column(DOUBLE, nullable=False)
            bid_max_price = Column(DOUBLE, nullable=False)
            ask_min_price = Column(DOUBLE, nullable=False)

            high_price = Column(DOUBLE, nullable=False)
            low_price = Column(DOUBLE, nullable=False)

        query_data = Tick()

        # Check table then create if not exist.
        if table_name not in query_data.metadata.bind.table_names():
            query_data.metadata.create_all(engine)

        query_data.close_price = float(tick_data['last_price'])
        query_data.trade_volume = float(tick_data['volume'])
        query_data.ticker_time = datetime.fromtimestamp(float(tick_data['timestamp'])) - timedelta(hours=9)

        query_data.high_price = float(tick_data['high'])
        query_data.low_price = float(tick_data['low'])

        query_data.mid_price = float(tick_data['mid'])
        query_data.bid_max_price = float(tick_data['bid'])
        query_data.ask_min_price = float(tick_data['ask'])
        # insert_data is not a coroutine function
        await loop.run_in_executor(None, insert_data, query_data)

    except Exception as e:
        log.error(e)
        raise


# noinspection PyPep8
async def request_consume(main, dlq, session, results):
    log = logging.getLogger("request_consume")

    while True:
        # wait for an item from the producer
        (pair, _try_count) = await main.get()

        if _try_count < 2:
            prox = haproxy['http'].format(11112)
        else:
            prox = random.choice(AWS_PROXIES)['https']

        try:
            async with session.get(TICK_URL.format(pair),
                                   proxy=prox) as resp:
                log.info("consuming request {}".format(resp.url))
                # async `json` method의 경우 content_type을 check해주는데, 여기서 에러가 날 수 있다.
                # Bitfinex 응답에는 Content_type을 application/json 이 포함되어 있어 aiohttp에서 에러나지 않음.
                data = resp.json(encoding='utf-8')
                _data = await data

                processed_data = await data_process(pair, _data)

                # for raw data redis saving?
                results.append({"Raw_Data_{}".format(pair): _data,
                                "Processed_Data_{}".format(pair): processed_data})

                main.task_done()

        except Exception as exp:
            """
            retry count를 배치해서 3번이상 실패하면 dlq queue 라인에 있는 것을 종결처리해버린다.
            """
            log.error(exp)
            _try_count += 1

            if _try_count < 3:
                log.info("Queue Error at {}, re-try count {}".format(pair.upper(), _try_count))

                await dlq.put((pair, _try_count))
                await asyncio.sleep(3)
                main.task_done()
            else:
                log.error("It more than 3 times retry, Done it")
                dlq.task_done()


async def request_produce(queue, pair_list):
    log = logging.getLogger("request_produce")

    for pair in pair_list:
        log.info('producing {} request'.format(pair.upper()))
        # put the item in the queue
        await queue.put((pair, 0))  # 0 is retry counter


async def request_order(session, pair_list):
    log = logging.getLogger("request_order")
    main_queue = asyncio.Queue()
    dlq_queue = asyncio.Queue()
    results = []

    await request_produce(queue=main_queue, pair_list=pair_list)

    # len(aws_proxies) 의 이유는 이것이 semaphore와 같은 역할을 한다.
    # requests 날리는 타이밍이 달라진다.
    consumers = [asyncio.ensure_future(request_consume(main=main_queue,
                                                       dlq=dlq_queue,
                                                       session=session,
                                                       results=results)) for _ in range(int(len(AWS_PROXIES) / 1))]

    dlq_consumers = [asyncio.ensure_future(request_consume(main=dlq_queue,
                                                           dlq=dlq_queue,
                                                           session=session,
                                                           results=results)) for _ in range(int(len(AWS_PROXIES) / 2))]

    log.info("tesks all completed")
    await main_queue.join()
    await dlq_queue.join()
    log.info("all queue joined")
    for _future in consumers + dlq_consumers:
        _future.cancel()


async def run(pair_list):
    log = logging.getLogger("Run")
    try:
        async with aiohttp.ClientSession(headers={'User-Agent': "Python/3.6"}) as session:
            await request_order(session, pair_list)
    except Exception as e:
        log.error(e)

################################
# Main section
################################
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(name)18s : %(message)s', stream=sys.stderr)
logging.basicConfig(level=logging.ERROR, format='%(asctime)s %(name)18s : %(message)s', stream=sys.stderr)
if __name__ == '__main__':
    start_time = datetime.utcnow()

    (_path, _file) = os.path.split(__file__)
    filename = [seg.upper() for seg in _file.split('_')]

    TICK_URL = "https://api.bitfinex.com/v1/pubticker/{}"
    TABLE_NAME = "_".join(filename[:2]).upper() + "_v1_{}"  # syncing file name as table.

    rootlog = logging.getLogger()

    # get pair_set from local redis server
    try: # get pair_set from local redis server
        r = redis.StrictRedis(host="localhost", port=6379, charset="utf-8", decode_responses=True, db=0)

        PAIR_LIST = r.smembers("vgbf_v1_pair:using_set")
        rootlog.info("all pairs count : {}".format(len(PAIR_LIST)))
    except Exception as e:
        rootlog.warning("redis db has problem", e)
        with open(Path.joinpath(_path, "pair_list.json"), "r", encoding='utf8') as pair_file:
            PAIR_LIST = ujson.loads(pair_file.read())

        rootlog.error("all pairs count : {} from 'pair_list.json'".format(len(PAIR_LIST)))


    rootlog.info("all pairs count : {}".format(len(PAIR_LIST)))
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(PAIR_LIST))

    # 후에 Redis서버에서 거래량을 비례하여 우선처리 순서를 정하면 좋을것 같다.
    end_time = datetime.utcnow()
    rootlog.info("total running time : {} ".format(str(end_time - start_time)))

