import sys
import ujson
import logging
from pathlib import Path

import redis



"""
Regular Dump json from pair information

1. search directories under exchanges
2. 
"""

log = logging.getLogger()
logging.basicConfig(level=logging.INFO, format='%(asctime)s : %(message)s', stream=sys.stderr)

try:
    r = redis.StrictRedis(host="localhost", port=6379, charset="utf-8", decode_responses=True, db=0)
    key_list = r.keys()
except Exception as e:
    log.error(e)
    raise ConnectionError

p = Path(".")
file_list = [f for f in p.glob("**/*.py")]

#queue_list = [{'key': 'vgbf', 'path': 'Bitfinex'}, ...]
queue_list = [{"path":_file._parts[0], "key":_file._parts[1].split("_")[0]}
              for _file in file_list if str(_file).endswith("pair.py")]

for queue in queue_list:
    _keys =  [_key for _key in key_list
              if _key.startswith(queue["key"]) and _key.endswith("using_set")]

    if _keys:
        pair_data = r.smembers(_keys[0])
    else:
        log.error("{} :_key does not exsit".format(str(queue)))
        raise FileExistsError

    with open(Path.joinpath(Path.cwd(), queue['path'], "pair_list.json"),
              "w", encoding='utf8') as f:
        f.write(ujson.dumps(pair_data))
    log.info("{} file dumped".format(_keys[0]))
